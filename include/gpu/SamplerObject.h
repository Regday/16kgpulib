#ifndef __K_GPU_SAMPLER_OBJECT_H__
#define __K_GPU_SAMPLER_OBJECT_H__
#include "gpu.h"

namespace k
{
	namespace gpu
	{
		class SamplerObject
		{
		public:
			SamplerObject();
			~SamplerObject();

			SamplerObject(SamplerObject&& that);
			SamplerObject& operator=(SamplerObject&& that);

			SamplerObject(const SamplerObject& that) = delete;
			SamplerObject& operator=(const SamplerObject&) = delete;

			// Wrapping
			void SetWrapMode(gpu_tex_wrapping wrapS, gpu_tex_wrapping wrapT, gpu_tex_wrapping wrapR = gpu_tex_wrapping::repeat);

			gpu_tex_wrapping GetWrapModeS() const;
			gpu_tex_wrapping GetWrapModeT() const;
			gpu_tex_wrapping GetWrapModeR() const;

			// Filtering
			void SetMagFilter(gpu_tex_filter filer);
			void SetMinFilter(gpu_tex_filter filter);

			gpu_tex_filter GetMagFilter() const;
			gpu_tex_filter GetMinFilter() const;

			// LOD
			void SetMinLod(int32 lod);
			void SetMaxLod(int32 lod);
			void SetLodBias(float bias);

			int32 GetMinLod() const;
			int32 GetMaxLod() const;
			float GetLodBias() const;

			// Anisotropy
			void	SetMaxAnisotropy(float aniso);
			float	GetMaxAnisotropy() const;

			// Binding
			void		Bind(gpu_tex_unit unit);
			static void Unbind(gpu_tex_unit unit);
			uint32		GetId() const;

		private:
			gpu_tex_wrapping _wrapS;
			gpu_tex_wrapping _wrapT;
			gpu_tex_wrapping _wrapR;

			gpu_tex_filter	_minFilter;
			gpu_tex_filter	_magFilter;

			int32			_minLod;
			int32			_maxLod;
			float			_lodBias;

			float			_maxAnisotropy;

			uint32			_id;
		};
	}
}
#endif