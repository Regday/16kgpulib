#ifndef _K_GPU_DEVICE_H_
#define _K_GPU_DEVICE_H_

#include <cstdint>
#include <cstdlib>
#include <utility>
#include <vector>

#include "glad/glad.h"

typedef uint8_t byte;

typedef int8_t	int8;
typedef int16_t int16;
typedef int32_t int32;
typedef int64_t int64;

typedef uint8_t	 uint8;
typedef uint16_t uint16;
typedef uint32_t uint32;
typedef uint64_t uint64;

typedef ptrdiff_t intptr;


#undef MessageBox
namespace k
{
	static void(*MessageBoxPtr)(const char*message, const char* header);

	static void MessageBox(const char*message, const char* header = "MESSAGE")
	{
		if (MessageBoxPtr)
			MessageBoxPtr(message, header);
	}

	namespace gpu
	{
		enum struct gpu_type : GLenum
		{
			int8 = GL_BYTE,
			int16 = GL_SHORT,
			int32 = GL_INT,
			uint8 = GL_UNSIGNED_BYTE,
			uint16 = GL_UNSIGNED_SHORT,
			uint32 = GL_UNSIGNED_INT,
			float32 = GL_FLOAT,
		};

		enum struct gpu_tex_unit : uint32
		{
			unit0 = 0,
			unit1,
			unit2,
			unit3,
			unit4,
			unit5,
			unit6,
			unit7,
		};

		enum struct gpu_tex_wrapping : GLenum
		{
			repeat = GL_REPEAT,
			clamp_to_edge = GL_CLAMP_TO_EDGE,
			clamp_to_border = GL_CLAMP_TO_BORDER,
			mirrored_repeat = GL_MIRRORED_REPEAT,
			//gpu_mirror_clamp = GL_MIRROR_CLAMP,
			//gpu_mirror_clamp_to_edge = GL_MIRROR_CLAMP_TO_EDGE,
			//gpu_mirror_clamp_to_border = GL_MIRROR_CLAMP_TO_BORDER
		};

		enum struct gpu_tex_filter : GLenum
		{
			nearest = GL_NEAREST,
			linear = GL_LINEAR,
			nearest_mipmap_nearest = GL_NEAREST_MIPMAP_NEAREST,
			linear_mipmap_nearest = GL_LINEAR_MIPMAP_NEAREST,
			nearest_mipmap_linear = GL_NEAREST_MIPMAP_LINEAR,
			linear_mipmap_linear = GL_LINEAR_MIPMAP_LINEAR,
		};

		inline void CheckError(const char* message)
		{
			GLenum err = glGetError();
			if (err == GL_INVALID_ENUM)
				k::MessageBox(message, "GL_INVALID_ENUM");
			else if (err == GL_INVALID_VALUE)
				k::MessageBox(message, "GL_INVALID_VALUE");
			else if (err == GL_INVALID_OPERATION)
				k::MessageBox(message, "GL_INVALID_OPERATION");
			//else if (err == GL_STACK_OVERFLOW)
			//	k::MessageBox(message, "GL_STACK_OVERFLOW");
			//else if (err == GL_STACK_UNDERFLOW)
			//	k::MessageBox(message, "GL_STACK_UNDERFLOW");
			else if (err == GL_OUT_OF_MEMORY)
				k::MessageBox(message, "GL_OUT_OF_MEMORY");
		}

		enum struct gpu_depth : GLenum
		{
			disable = 0,
			never = GL_NEVER,
			less = GL_LESS,
			equal = GL_EQUAL,
			lequal = GL_LEQUAL,
			greater = GL_GREATER,
			notequal = GL_NOTEQUAL,
			gequal = GL_GEQUAL,
			always = GL_ALWAYS,
		};

		enum struct gpu_cull_face : GLenum
		{
			disable = 0,
			back = GL_BACK,
			front = GL_FRONT,
			front_and_back = GL_FRONT_AND_BACK,
		};

		enum struct gpu_scissor : GLenum
		{
			disable = 0,
			enable = 1,
		};

		enum struct gpu_blend : GLenum
		{
			disable = 0,
			add = GL_FUNC_ADD,
			sub = GL_FUNC_SUBTRACT,
			reverse_sub = GL_FUNC_REVERSE_SUBTRACT,
			min = GL_MIN,
			max = GL_MAX,
		};

		enum struct gpu_blend_sfactor : GLenum
		{
			zero = GL_ZERO,
			one = GL_ONE,
			src_color = GL_SRC_COLOR,
			one_minus_src_color = GL_ONE_MINUS_SRC_COLOR,
			dst_color = GL_DST_COLOR,
			one_minus_dst_color = GL_ONE_MINUS_DST_COLOR,
			src_alpha = GL_SRC_ALPHA,
			one_minus_src_alpha = GL_ONE_MINUS_SRC_ALPHA,
			dst_alpha = GL_DST_ALPHA,
			one_minus_dst_alpha = GL_ONE_MINUS_DST_ALPHA,
			constant_color = GL_CONSTANT_COLOR,
			one_minus_constant_color = GL_ONE_MINUS_CONSTANT_COLOR,
			constant_alpha = GL_CONSTANT_ALPHA,
			one_minus_constant_alpha = GL_ONE_MINUS_CONSTANT_ALPHA,
			src_alpha_saturate = GL_SRC_ALPHA_SATURATE,
		};

		enum struct gpu_blend_dfactor : GLenum
		{
			zero = GL_ZERO,
			one = GL_ONE,
			src_color = GL_SRC_COLOR,
			one_minus_src_color = GL_ONE_MINUS_SRC_COLOR,
			dst_color = GL_DST_COLOR,
			one_minus_dst_color = GL_ONE_MINUS_DST_COLOR,
			src_alpha = GL_SRC_ALPHA,
			one_minus_src_alpha = GL_ONE_MINUS_SRC_ALPHA,
			dst_alpha = GL_DST_ALPHA,
			one_minus_dst_alpha = GL_ONE_MINUS_DST_ALPHA,
			constant_color = GL_CONSTANT_COLOR,
			one_minus_constant_color = GL_ONE_MINUS_CONSTANT_COLOR,
			constant_alpha = GL_CONSTANT_ALPHA,
			one_minus_constant_alpha = GL_ONE_MINUS_CONSTANT_ALPHA,
		};

		struct gpu_state
		{
			gpu_state() : depth(gpu_depth::less), cull_face(gpu_cull_face::back),
				scissor(gpu_scissor::disable), blend(gpu_blend::disable),
				blend_sfactor(gpu_blend_sfactor::one), blend_dfactor(gpu_blend_dfactor::zero)
			{}
			gpu_depth depth;
			gpu_cull_face cull_face;
			gpu_scissor scissor;
			gpu_blend blend;
			gpu_blend_sfactor blend_sfactor;
			gpu_blend_dfactor blend_dfactor;
		};

		inline void SetState(const gpu_state& state)
		{
			if (state.depth == gpu_depth::disable)
				glDisable(GL_DEPTH_TEST);
			else
			{
				glEnable(GL_DEPTH_TEST);
				glDepthFunc((GLenum)state.depth);
			}

			if (state.cull_face == gpu_cull_face::disable)
				glDisable(GL_CULL_FACE);
			else
			{
				glEnable(GL_CULL_FACE);
				glCullFace((GLenum)state.cull_face);
			}

			if (state.scissor == gpu_scissor::disable)		
				glDisable(GL_SCISSOR_TEST);
			else
				glEnable(GL_SCISSOR_TEST);

			if (state.blend == gpu_blend::disable)
				glDisable(GL_BLEND);
			else
			{
				glEnable(GL_BLEND);
				glBlendEquation((GLenum)state.blend);
				glBlendFunc((GLenum)state.blend_sfactor, (GLenum)state.blend_dfactor);
			}
		}

		inline void Scissor(int32 x, int32 y, int32 w, int32 h)
		{
			glScissor(x, y, w, h);
		}

		inline void Viewport(int32 x, int32 y, int32 w, int32 h)
		{
			glViewport(x, y, w, h);
		}

		inline void ClearColor(float r, float g, float b, float a)
		{
			glClearColor(r, g, b, a);
		}

		enum struct gpu_clear_flags : GLbitfield
		{
			color = GL_COLOR_BUFFER_BIT,
			depth = GL_DEPTH_BUFFER_BIT,
			depth_and_color = GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT,
		};

		inline void Clear(gpu_clear_flags flags)
		{
			glClear((GLbitfield)flags);
		}
	}

}


#endif // !_K_DEVICE_DEVICE_H_