#include "gpu/BufferObject.h"

using namespace k;
using namespace gpu;

BufferObject::BufferObject() { glCreateBuffers(1, &_id); }

BufferObject::~BufferObject() {	if(_id) glDeleteBuffers(1, &_id); }

BufferObject::BufferObject(BufferObject&& that)
{
	_id = that._id;
	_target = that._target;

	that._id = 0;
}

BufferObject& BufferObject::operator=(BufferObject&& that)
{
	std::swap(_id, that._id);
	std::swap(_target, that._target);
	return *this;
}

void BufferObject::Allocate(size_t size, gpu_buffer_target target, gpu_buffer_usage usageMode, const void* data)
{
	_target = target;
	glNamedBufferData(_id, size, data, (GLenum)usageMode);
}

void BufferObject::LoadData(intptr offset, size_t size, const void* data)
{
	glNamedBufferSubData(_id, offset, size, data);
}

void BufferObject::Bind() { glBindBuffer((GLenum)_target, _id); }

void BufferObject::Unbind(gpu_buffer_target target) { glBindBuffer((GLenum)target, 0); }

void BufferObject::BindToIndexedBuffer(uint32 bufferUnit) { glBindBufferBase((GLenum)_target, bufferUnit, this->_id); }

uint32 BufferObject::GetId() const { return _id; }