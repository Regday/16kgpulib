#include "gpu/SamplerObject.h"
using namespace k;
using namespace gpu;

SamplerObject::SamplerObject()
{
	glGenSamplers(1, &_id);

	glGetSamplerParameteriv(_id, GL_TEXTURE_WRAP_S, (GLint*)&_wrapS);
	glGetSamplerParameteriv(_id, GL_TEXTURE_WRAP_T, (GLint*)&_wrapT);
	glGetSamplerParameteriv(_id, GL_TEXTURE_WRAP_R, (GLint*)&_wrapR);

	glGetSamplerParameteriv(_id, GL_TEXTURE_MAG_FILTER, (GLint*)&_magFilter);
	glGetSamplerParameteriv(_id, GL_TEXTURE_MIN_FILTER, (GLint*)&_minFilter);

	glGetSamplerParameteriv(_id, GL_TEXTURE_MIN_LOD, (GLint*)&_minLod);
	glGetSamplerParameteriv(_id, GL_TEXTURE_MAX_LOD, (GLint*)&_maxLod);
	glGetSamplerParameterfv(_id, GL_TEXTURE_LOD_BIAS, (GLfloat*)&_lodBias);

	// glGetSamplerParameterfv(_id, GL_TEXTURE_MAX_ANISOTROPY_ARB, (GLfloat*)&_maxAnisotropy);
}

SamplerObject::~SamplerObject()
{
	glDeleteSamplers(1, &_id);
}

SamplerObject::SamplerObject(SamplerObject&& that)
{
	_id = that._id;
	std::swap(_wrapS, that._wrapS);
	std::swap(_wrapT, that._wrapT);
	std::swap(_wrapR, that._wrapR);

	std::swap(_minFilter, that._minFilter);
	std::swap(_magFilter, that._magFilter);

	std::swap(_minLod, that._minLod);
	std::swap(_maxLod, that._maxLod);
	std::swap(_lodBias, that._lodBias);
	std::swap(_maxAnisotropy, that._maxAnisotropy);

	that._id = 0;
}

SamplerObject& SamplerObject::operator=(SamplerObject&& that)
{
	std::swap(_id, that._id);

	std::swap(_wrapS, that._wrapS);
	std::swap(_wrapT, that._wrapT);
	std::swap(_wrapR, that._wrapR);

	std::swap(_minFilter, that._minFilter);
	std::swap(_magFilter, that._magFilter);

	std::swap(_minLod, that._minLod);
	std::swap(_maxLod, that._maxLod);
	std::swap(_lodBias, that._lodBias);
	std::swap(_maxAnisotropy, that._maxAnisotropy);

	return *this;
}


void SamplerObject::SetWrapMode(gpu_tex_wrapping wrapS, gpu_tex_wrapping wrapT, gpu_tex_wrapping wrapR)
{
	_wrapS = wrapS;
	_wrapT = wrapT;
	_wrapR = wrapR;

	glSamplerParameteri(_id, GL_TEXTURE_WRAP_S, (GLint)_wrapS);
	glSamplerParameteri(_id, GL_TEXTURE_WRAP_T, (GLint)_wrapT);
	glSamplerParameteri(_id, GL_TEXTURE_WRAP_R, (GLint)_wrapR);
}

gpu_tex_wrapping SamplerObject::GetWrapModeS() const {
	return _wrapS;
}

gpu_tex_wrapping SamplerObject::GetWrapModeT() const {
	return _wrapT;
}

gpu_tex_wrapping SamplerObject::GetWrapModeR() const {
	return _wrapR;
}

void SamplerObject::SetMagFilter(gpu_tex_filter filter) {
	_magFilter = filter;
	glSamplerParameteri(_id, GL_TEXTURE_MAG_FILTER, (GLint)_magFilter);
}

void SamplerObject::SetMinFilter(gpu_tex_filter filter) {
	_minFilter = filter;
	glSamplerParameteri(_id, GL_TEXTURE_MIN_FILTER, (GLint)_minFilter);
}

gpu_tex_filter SamplerObject::GetMagFilter() const {
	return _magFilter;
}

gpu_tex_filter SamplerObject::GetMinFilter() const {
	return _minFilter;
}

void SamplerObject::SetMinLod(int32 lod) {
	_minLod = lod;
	glSamplerParameteri(_id, GL_TEXTURE_MIN_LOD, _minLod);
}

void SamplerObject::SetMaxLod(int32 lod) {
	_maxLod = lod;
	glSamplerParameteri(_id, GL_TEXTURE_MAX_LOD, _maxLod);
}

void SamplerObject::SetLodBias(float bias) {
	_lodBias = bias;
	glSamplerParameterf(_id, GL_TEXTURE_LOD_BIAS, _lodBias);
}

int32 SamplerObject::GetMinLod() const {
	return _minLod;
}

int32 SamplerObject::GetMaxLod() const {
	return _maxLod;
}

float SamplerObject::GetLodBias() const {
	return _lodBias;
}

void SamplerObject::SetMaxAnisotropy(float aniso) {
	_maxAnisotropy = aniso;
	// glSamplerParameterfv(_id, GL_TEXTURE_MAX_ANISOTROPY_ARB, (GLfloat*)&_maxAnisotropy);
}

float SamplerObject::GetMaxAnisotropy() const {
	return _maxAnisotropy;
}

void SamplerObject::Bind(gpu_tex_unit unit) {
	glBindSampler((GLuint)unit, _id);
}

void SamplerObject::Unbind(gpu_tex_unit unit) {
	glBindSampler((GLuint)unit, 0);
}

uint32 SamplerObject::GetId() const
{
	return _id;
}