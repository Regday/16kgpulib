#include "gpu/PipelineObject.h"

using namespace k;
using namespace gpu;

Shader::Shader(gpu_shader_type type, const char* src)
{
	_type = type;
	_id = glCreateShaderProgramv((GLenum)type, 1, &src);

	int32 isLinked = 0;
	glGetProgramiv(_id, GL_LINK_STATUS, (int *)&isLinked);
	if (!isLinked)
	{
		int32 maxLength = 0;
		glGetProgramiv(_id, GL_INFO_LOG_LENGTH, &maxLength);

		char* log = new char[maxLength];
		glGetProgramInfoLog(_id, maxLength, &maxLength, log);
		k::MessageBox(log, "Shader error");

		if (_id)
		{
			glDeleteProgram(_id);
			_id = 0;
		}
	}
}

Shader::~Shader() { if (_id) glDeleteShader(_id); }

Shader::Shader(Shader&& that)
{
	_id = that._id;
	_type = that._type;

	that._id = 0;
}

Shader& Shader::operator=(Shader&& that)
{
	std::swap(_id, that._id);
	std::swap(_type, that._type);
	return *this;
}


uint32 Shader::GetID() const { return _id; }

gpu_shader_type Shader::GetType() const { return _type; }

void Shader::BindShaderStorage(const char* blockName, uint32 bufferUnit)
{
	uint32 block_index = glGetProgramResourceIndex(_id, GL_SHADER_STORAGE_BLOCK, blockName);
	glShaderStorageBlockBinding(_id, block_index, bufferUnit);
}

void Shader::BindUniformBuffer(const char* blockName, uint32 bufferUnit)
{
	uint32 uniformBlock = glGetUniformBlockIndex(_id, blockName);
	glUniformBlockBinding(_id, uniformBlock, bufferUnit);
}

PipelineObject::PipelineObject() { glCreateProgramPipelines(1, &_id); }

PipelineObject::~PipelineObject() { if(_id) glDeleteProgramPipelines(1, &_id); }

PipelineObject::PipelineObject(PipelineObject&& that)
{
	_id = that._id;

	that._id = 0;
}

PipelineObject& PipelineObject::operator=(PipelineObject&& that)
{
	std::swap(_id, that._id);
	return *this;
}


void PipelineObject::AttachShader(const Shader &shader)
{
	gpu_shader_bitfield bit;
	gpu_shader_type type = shader.GetType();
	switch (type)
	{
	case gpu_shader_type::vertex:
		bit = gpu_shader_bitfield::vertex;
		break;
	case gpu_shader_type::fragment:
		bit = gpu_shader_bitfield::fragment;
		break;
	case gpu_shader_type::geometry:
		bit = gpu_shader_bitfield::geometry;
		break;
	default:
		break;
	}

	glUseProgramStages(_id, (GLbitfield)bit, shader.GetID());
}

void PipelineObject::DetachShader(gpu_shader_type type)
{
	gpu_shader_bitfield bit;
	switch (type)
	{
	case gpu_shader_type::vertex:
		bit = gpu_shader_bitfield::vertex;
		break;
	case gpu_shader_type::fragment:
		bit = gpu_shader_bitfield::fragment;
		break;
	case gpu_shader_type::geometry:
		bit = gpu_shader_bitfield::geometry;
		break;
	default:
		break;
	}

	glUseProgramStages(_id, (GLbitfield)bit, 0);
}

bool PipelineObject::Validate()
{
	int status;
	glValidateProgramPipeline(_id);
	glGetProgramPipelineiv(_id, GL_VALIDATE_STATUS, &status);

	if (!status)
	{
		int32 maxLength = 0;
		glGetProgramPipelineiv(_id, GL_INFO_LOG_LENGTH, &maxLength);

		char* log = new char[maxLength];
		glGetProgramPipelineInfoLog(_id, maxLength, &maxLength, log);
		k::MessageBox(log, "Pipeline error");
	}

	return (bool)status;
}

void PipelineObject::Bind()
{
	glBindProgramPipeline(_id);
}

void PipelineObject::Unbind()
{
	glBindProgramPipeline(0);
}
